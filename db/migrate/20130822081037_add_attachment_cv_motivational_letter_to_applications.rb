class AddAttachmentCvMotivationalLetterToApplications < ActiveRecord::Migration
  def self.up
    change_table :applications do |t|
      t.attachment :cv
      t.attachment :motivational_letter
    end
  end

  def self.down
    drop_attached_file :applications, :cv
    drop_attached_file :applications, :motivational_letter
  end
end
