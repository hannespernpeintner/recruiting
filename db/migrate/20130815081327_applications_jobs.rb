class ApplicationsJobs < ActiveRecord::Migration
  def up
    create_table :applications_jobs, :id => false do |t|
      t.references :application, :null => false
      t.references :job, :null => false
    end
  end

  def down
  end
end
