class DropCvAndMotivationalLetterFromUser < ActiveRecord::Migration
  def up
     remove_column :users, :cv
     remove_column :users, :motivational_letter
  end

  def down
  end
end
