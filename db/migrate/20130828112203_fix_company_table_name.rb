class FixCompanyTableName < ActiveRecord::Migration
  def up
    rename_table :companiess_jobs, :companies_jobs
  end

  def down
    rename_table :companies_jobs, :companiess_jobs
  end
end
