class ChangeContactColumnName < ActiveRecord::Migration
  def up
    rename_column :jobs, :contact, :contact_id
  end

  def down
  end
end
