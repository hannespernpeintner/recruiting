class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.text :template_header
      t.text :template_footer

      t.timestamps
    end
  end
end
