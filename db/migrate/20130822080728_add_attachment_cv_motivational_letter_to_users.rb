class AddAttachmentCvMotivationalLetterToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :cv
      t.attachment :motivational_letter
    end
  end

  def self.down
    drop_attached_file :users, :cv
    drop_attached_file :users, :motivational_letter
  end
end
