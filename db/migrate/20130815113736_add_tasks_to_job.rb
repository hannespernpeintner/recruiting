class AddTasksToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :tasks, :text
  end
end
