class RenameColumnCompanyId < ActiveRecord::Migration
  def up
    rename_column :jobs, :companyid, :company_id
  end

  def down
  end
end
