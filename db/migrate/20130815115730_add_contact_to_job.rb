class AddContactToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :contact, :integer
  end
end
