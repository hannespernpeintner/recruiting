class AddStreetAndZipAndCityToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :street, :string
    add_column :companies, :zip, :integer
    add_column :companies, :city, :string
  end
end
