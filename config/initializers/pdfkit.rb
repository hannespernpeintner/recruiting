PDFKit.configure do |config|
  config.wkhtmltopdf = `which wkhtmltopdf`.to_s.strip
  config.default_options = {
    :ignore_load_errors=>true,
    :encoding=>"UTF-8",
    :page_size=>"A4",
    :margin_top=>"0.25in",
    :margin_right=>"0.35in",
    :margin_bottom=>"0.25in",
    :margin_left=>"0.35in",
    :disable_smart_shrinking=>false
    }
end