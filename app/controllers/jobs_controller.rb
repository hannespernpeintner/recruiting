class JobsController < ApplicationController
  skip_before_filter :authenticate_user!
  load_and_authorize_resource
  # GET /jobs
  # GET /jobs.json
  def index
    # if scope is given, we have to check if the user may see other publications than published ones
    scope = params[:scope]
    if scope && scope != :published && can?(:modify, Job)
      @jobs = Job.accessible_by(current_ability).send(scope)
    else
      @jobs = Job.accessible_by(current_ability)
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @jobs }
    end
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
    @job = Job.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @job }
    end
  end
  
  def pdf
    @job = Job.find(params[:id])
    
    html = render_to_string(:action => "template.html.erb", :layout => false)
    kit = PDFKit.new(html)
    kit.stylesheets << "#{Rails.root}/app/assets/stylesheets/bootstrap_and_overrides.css"
    kit.stylesheets << "#{Rails.root}/app/assets/stylesheets/bootstrap.min.css"
    send_data(kit.to_pdf, :filename => 'job.pdf', :type => 'application/pdf', :disposition => 'inline')
    
  end

  # GET /jobs/new
  # GET /jobs/new.json
  def new
    @job = Job.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @job }
    end
  end

  # GET /jobs/1/edit
  def edit
    @job = Job.find(params[:id])
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(params[:job])

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: 'Job was successfully created.' }
        format.json { render json: @job, status: :created, location: @job }
      else
        format.html { render action: "new" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  def transition
    notice = ""
    event = params[:event]
    if @job.send(params[:event])
        notice = event.humanize + " was successfull."
      else
        notice = event.humanize + " wasn't successful."
    end
    
    redirect_to root_path, notice: notice
  end
  
  # PUT /jobs/1
  # PUT /jobs/1.json
  def update
    @job = Job.find(params[:id])

    respond_to do |format|
      if @job.update_attributes(params[:job])
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job = Job.find(params[:id])
    @job.destroy

    respond_to do |format|
      format.html { redirect_to jobs_url }
      format.json { head :no_content }
    end
  end
end
