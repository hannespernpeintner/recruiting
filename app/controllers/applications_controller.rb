class ApplicationsController < ApplicationController
  # GET /applications
  # GET /applications.json
  def index
    scope = params[:scope]
     if scope && scope != :published && can?(:modify, Application)
      @applications = Application.accessible_by(current_ability).send(scope)
    else
      @applications = Application.accessible_by(current_ability)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @applications }
    end
  end
  
  def transition
    @application = Application.find(params[:id])
    notice = ""
    event = params[:event]
    if @application.send(params[:event])
        notice = event.humanize + " was successfull."
        if event == :publish.to_s
          ApplicationMailer.acknowledgement_email(current_user, Job.find(@application.job_id)).deliver
        end
      else
        notice = event.humanize + " wasn't successful."
    end
    
    redirect_to applications_path, notice: notice
  end
  
  def own
    @applications = current_user.applications

    respond_to do |format|
      format.html { render template: "applications/index" }
      format.json { render json: @applications }
    end
  end

  # GET /applications/1
  # GET /applications/1.json
  def show
    @application = Application.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @application }
    end
  end

  # GET /applications/new
  # GET /applications/new.json
  def new
    @application = Application.new
    @application.job_id = params['job_id'] if params['job_id']
    @application.user_id = current_user.id
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @application }
    end
  end

  # GET /applications/1/edit
  def edit
    @application = Application.find(params[:id])
  end

  # POST /applications
  # POST /applications.json
  def create
    @application = Application.new(params[:application])

    respond_to do |format|
      if @application.save
        format.html { redirect_to @application, notice: 'Application was successfully created.' }
        format.json { render json: @application, status: :created, location: @application }
      else
        format.html { render action: "new" }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  def publish
    @application = Application.find(params[:id])
    notice = ""
    if @application.send(params[:event])
        notice = "Application was successfully published."
      else
        notice = "Application can't be published."
    end
    
    redirect_to root_path, notice: notice
  end
  
  # PUT /applications/1
  # PUT /applications/1.json
  def update
    @application = Application.find(params[:id])

    respond_to do |format|
      if @application.update_attributes(params[:application])
        format.html { redirect_to @application, notice: 'Application was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /applications/1
  # DELETE /applications/1.json
  def destroy
    @application = Application.find(params[:id])
    @application.destroy

    respond_to do |format|
      format.html { redirect_to applications_url }
      format.json { head :no_content }
    end
  end
end
