class NavbarLoginAreaCell < Cell::Rails
  include CanCan::ControllerAdditions
  include Devise::Controllers::Helpers
  Devise::Controllers::Helpers.define_helpers(Devise::Mapping.new(:user, {}))
  Devise::Controllers::Helpers.define_helpers(User)
  helper_method :current_user
  def display
    render
  end

end
