class Job < ActiveRecord::Base
  attr_accessible :description, :title, :tasks, :qualifications, :company_id, :company, :contact_id, :contact, :state, :tag_ids, :tags, :applications
  belongs_to :company
  belongs_to :user
  has_many :applications
  has_and_belongs_to_many :tags
  accepts_nested_attributes_for :tags
  
  scope :reviewed, -> { where(state: ["reviewed", "review_requested"]) }
  scope :draft, -> { where(state: ["created"]) }
  scope :published, -> { where(state: ["published"]) }
  
  validates :contact_id, :company_id, :presence => true
  
  state_machine :initial => :created do
    
    before_transition :created => :published, :do => :print_published
    
    event :request_review do
      transition :created => :review_requested
    end
    
    event :review do
      transition :review_requested => :reviewed
    end
    
    event :publish do
      transition :reviewed => :published
    end
    
  end
  
  def print_published
    puts "publiiiiiiiiiiished"
  end
  
  
  def contact
    if contact_id
      User.find(contact_id)
    else
      User.find(Company.find(company_id).contact_id) if company_id
    end 
  end
end