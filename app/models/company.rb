class Company < ActiveRecord::Base
  attr_accessible :name, :street, :zip, :city, :contact_id, :template_footer, :template_header, :contact
  has_one :user
  has_many :jobs
  
  def address
    if street != "" && street != nil
      "#{street}, #{zip} #{city}"
    elsif zip != "" && zip != nil && city != "" && city != nil
      "#{zip} #{city}"
    else
      ""
    end
  end
end
