class Application < ActiveRecord::Base
  attr_accessible :motivation, :user_id, :job_id, :cv, :motivational_letter,
                  :cv_file_name, :cv_file_size, :cv_content_type, :cv_uploaded_at,
                  :motivational_letter_file_name, :motivational_letter_file_size,
                  :motivational_letter_content_type, :motivational_letter_uploaded_at
  has_attached_file :cv
  has_attached_file :motivational_letter
  
  validates_attachment :cv, :presence => true,
  :content_type => { :content_type => ["application/pdf", "application/doc", "application/docx", "application/odf"] },
  :size => { :in => 0..1000.kilobytes }
  validates_attachment :motivational_letter, :presence => true,
  :content_type => { :content_type => ["application/pdf", "application/doc", "application/docx", "application/odf"] },
  :size => { :in => 0..1000.kilobytes }
  
  
  belongs_to :job
  belongs_to :user
  
  scope :owned, -> { where(user_id: [current_user.id]) }
  scope :draft, -> { where(state: ["created"]) }
  scope :published, -> { where(state: ["published"]) }
  scope :released, -> { where(state: ["released"]) }
  scope :rated, -> { where(state: ["recommended", "dissuaded"]) }
  scope :hired, -> { where(state: ["hired"]) }
  scope :declined, -> { where(state: ["declined"]) }
  
  state_machine :initial => :created do
    
    #after_transition :created => :published, :do => :send_ack_mail
    
    event :publish do
      transition :created => :published
    end
    
    event :release do
      transition :published => :released
    end
    
    event :recommend do
      transition :released => :recommended
    end
    
    event :dissuade do
      transition :released => :dissuaded
    end
    
    event :hire do
      transition :recommended => :hired
    end
    
    event :decline do
      transition [:dissuaded, :recommended] => :declined
    end
  end
  
  def send_ack_mail
    ApplicationMailer.acknowledgement_email(current_user, Job.find(@application.job_id)).deliver
  end
end
