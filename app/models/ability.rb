class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
    if user
      
      if user.role == "admin"
        
        can :manage, :all

        can :read, Job
        can :pdf, Job
        cannot :modify, Job
        cannot :request_review, Job
        cannot :review, Job
        cannot :apply, Job

        cannot :modify, Company
        
        cannot :manage, Application
        cannot :publish, Application
        
      elsif user.role == "hr"

        can :manage, Job, :state => ['created', 'reviewed', 'published']
        can :read, Job, :state => ['created', 'reviewed', 'published']
        can :pdf, Job
        cannot :review, Job
        cannot :apply, Job

        can :manage, Application
        can :transition, Application
        can :publish, Application
        cannot :read, Application, :state => ['created', 'released']
        cannot :recommend, Application

        can :manage, Company

        can :read, User
        
      elsif user.role == "manager"
        
        can :read, Job, :state => ['review_requested']
        can :transition, Job, :state => ['review_requested']
        can :review, Job, :state => ['review_requested']
        can :pdf, Job
        cannot :modify, Job
        cannot :request_review, Job
        cannot :apply, Job

        can :read, Application, :state => ['released']
        can :recommend, Application, :state => ['released']
        can :dissuade, Application, :state => ['released']
        cannot :modify, Application
      else
        
        can :read, Job, :state => ['published']
        can :apply, Job, :state => ['published']
        can :pdf, Job
        
        can :publish, Application
        
        #Application.where(:user_id => user.id) do |app|
        #  can :manage, app
        #end
        
      end
      
    else
      
        can :read, Job, :state => ['published']
        can :apply, Job, :state => ['published']
        can :pdf, Job
        
        can :publish, Application
        
    end
  end
end
