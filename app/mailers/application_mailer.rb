class ApplicationMailer < ActionMailer::Base
  
  default from: 'recruiting@example.com'
 
  def acknowledgement_email(user, job)
    @user = user
    @url  = root_path
    @job = job
    mail(to: @user.email, subject: 'Your application has been placed successfully')
  end

end
