describe "Job" do
  context "is new and" do
    before(:each) do
      @job = FactoryGirl.build(:job_created)
    end
    
    it "review can be requested" do
      @job.request_review.should be_true
    end
    it "cannot be reviewed" do
      @job.review.should be_false
    end
    it "cannot be published" do
      @job.publish.should be_false
    end
  end
  
  context "is requested to be reviewed" do
    before(:each) do
      @job = FactoryGirl.build(:job_review_requested)
    end
    
    it "and review cannot be requested" do
      @job.request_review.should be_false
    end
    it "and can be reviewed" do
      @job.review.should be_true
    end
    it "and cannot be published" do
      @job.publish.should be_false
    end
  end
  
end
