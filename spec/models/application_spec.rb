describe "Application" do
  context "is new and" do
    before(:each) do
      @application = FactoryGirl.build(:application_created)
    end
    
    it "can be published" do
      @application.publish.should be_true
    end
    it "cannot be released" do
      @application.release.should be_false
    end
    it "cannot be recommended" do
      @application.recommend.should be_false
    end
    it "cannot be dissuaded" do
      @application.dissuade.should be_false
    end
    it "cannot be hired" do
      @application.hire.should be_false
    end
    it "cannot be declined" do
      @application.decline.should be_false
    end
  end
  
  context "is published and" do
    before(:each) do
      @application = FactoryGirl.build(:application_published)
    end
    
    it "can be released" do
      @application.release.should be_true
    end
    it "cannot be republished" do
      @application.publish.should be_false
    end
    it "cannot be recommended" do
      @application.recommend.should be_false
    end
    it "cannot be dissuaded" do
      @application.dissuade.should be_false
    end
    it "cannot be hired" do
      @application.hire.should be_false
    end
    it "cannot be declined" do
      @application.decline.should be_false
    end
  end
  
  
  context "is released and" do
    before(:each) do
      @application = FactoryGirl.build(:application_released)
    end
    
    it "cannot be rereleased" do
      @application.release.should be_false
    end
    it "cannot be republished" do
      @application.publish.should be_false
    end
    it "can be recommended" do
      @application.recommend.should be_true
    end
    it "can be dissuaded" do
      @application.dissuade.should be_true
    end
    it "cannot be hired" do
      @application.hire.should be_false
    end
    it "cannot be declined" do
      @application.decline.should be_false
    end
  end
  
  
  context "is recommended and" do
    before(:each) do
      @application = FactoryGirl.build(:application_recommended)
    end
    
    it "cannot be rereleased" do
      @application.release.should be_false
    end
    it "cannot be republished" do
      @application.publish.should be_false
    end
    it "cannot be recommended" do
      @application.recommend.should be_false
    end
    it "cannot be dissuaded" do
      @application.dissuade.should be_false
    end
    it "can be hired" do
      @application.hire.should be_true
    end
    it "can be declined" do
      @application.decline.should be_true
    end
  end
  
  
  context "is dissuaded and" do
    before(:each) do
      @application = FactoryGirl.build(:application_dissuaded)
    end
    
    it "cannot be rereleased" do
      @application.release.should be_false
    end
    it "cannot be republished" do
      @application.publish.should be_false
    end
    it "cannot be recommended" do
      @application.recommend.should be_false
    end
    it "cannot be dissuaded" do
      @application.dissuade.should be_false
    end
    it "cannot be hired" do
      @application.hire.should be_false
    end
    it "can be declined" do
      @application.decline.should be_true
    end
  end
  
  context "is hired and" do
    before(:each) do
      @application = FactoryGirl.build(:application_hired)
    end
    
    it "cannot be rereleased" do
      @application.release.should be_false
    end
    it "cannot be republished" do
      @application.publish.should be_false
    end
    it "cannot be recommended" do
      @application.recommend.should be_false
    end
    it "cannot be dissuaded" do
      @application.dissuade.should be_false
    end
    it "cannot be hired" do
      @application.hire.should be_false
    end
    it "cannot be declined" do
      @application.decline.should be_false
    end
  end
  
  context "is declined and" do
    before(:each) do
      @application = FactoryGirl.build(:application_declined)
    end
    
    it "cannot be rereleased" do
      @application.release.should be_false
    end
    it "cannot be republished" do
      @application.publish.should be_false
    end
    it "cannot be recommended" do
      @application.recommend.should be_false
    end
    it "cannot be dissuaded" do
      @application.dissuade.should be_false
    end
    it "cannot be hired" do
      @application.hire.should be_false
    end
    it "cannot be declined" do
      @application.decline.should be_false
    end
  end
  
end
