describe "Applicant" do
  before(:all) do
    @applicant = FactoryGirl.build(:applicant)
  end
  
  it "has correct abilities" do
    
    ability = Ability.new(@applicant)
    
    ability.should be_able_to(:read, Job)
    
    ability.should_not be_able_to(:manage, Job)
    ability.should_not be_able_to(:read, Company)

    ability.should be_able_to(:publish, Application)
  end
  
end

describe "HR" do
  before(:all) do
    @hr = FactoryGirl.build(:hr)
  end
  
  it "has correct abilities" do
    
    ability = Ability.new(@hr)
    
    ability.should be_able_to(:manage, Job)
    ability.should be_able_to(:request_review, Job)
    ability.should_not be_able_to(:review, Job)
    
    ability.should be_able_to(:manage, Company)
    
    ability.should_not be_able_to(:manage, User)
    ability.should be_able_to(:read, User)

    ability.should be_able_to(:publish, Application)
    
  end
  
end

describe "Admin" do
  before(:all) do
    @admin = FactoryGirl.build(:admin)
  end
  
  it "has correct abilities" do
    
    ability = Ability.new(@admin)
    
    ability.should be_able_to(:manage, User)
    
    ability.should_not be_able_to(:modify, Job)
    ability.should_not be_able_to(:review, Job)
    ability.should_not be_able_to(:request_review, Job)
    ability.should_not be_able_to(:apply, Job)
    
    ability.should_not be_able_to(:modify, Company)
    
    ability.should_not be_able_to(:publish, Application)    
    
  end
end

describe "Manager" do
  before(:all) do
    @manager = FactoryGirl.build(:manager)
  end
  
  it "has correct abilities" do
    
    ability = Ability.new(@manager)
    
    ability.should_not be_able_to(:manage, User)
    
    ability.should be_able_to(:review, Job)
    ability.should_not be_able_to(:modify, Job)
    ability.should_not be_able_to(:request_review, Job)
    ability.should_not be_able_to(:apply, Job)
    
    ability.should_not be_able_to(:modify, Company)

    ability.should_not be_able_to(:publish, Application)    
  end
end
