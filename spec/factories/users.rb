FactoryGirl.define do
  
  factory :applicant, :class => User do |user|
    user.email "applicant@test.de"
    user.password "start01"
    user.role "applicant"
  end
  
  factory :admin, :class => User do |user|
    user.email "admin@test.de"
    user.password "start01"
    user.role "admin"
  end
  
  factory :hr, :class => User do |user|
    user.email "hr@test.de"
    user.password "start01"
    user.role "hr"
  end
  
  factory :manager, :class => User do |user|
    user.email "manager@test.de"
    user.password "start01"
    user.role "manager"
  end
  
  factory :user do |user|
    user.email "hr@test.de"
    user.password "start01"
    user.role "hr"
  end

end