FactoryGirl.define do
  
  factory :application_created, :class => Application do |application|
    application.user_id 1
    application.job_id 1
    application.motivation "motivation" + Time.now.to_s
    application.state "created"
    application.cv_file_name "../Try It Out Doc.pdf"
    application.motivational_letter_file_name "../Try It Out Doc.pdf"
  end
  
  factory :application_published, :class => Application do |application|
    application.user_id 1
    application.job_id 1
    application.motivation "motivation" + Time.now.to_s
    application.state "published"
    application.cv_file_name "../Try It Out Doc.pdf"
    application.motivational_letter_file_name "../Try It Out Doc.pdf"
  end
  
  factory :application_released, :class => Application do |application|
    application.user_id 1
    application.job_id 1
    application.motivation "motivation" + Time.now.to_s
    application.state "released"
    application.cv_file_name "../Try It Out Doc.pdf"
    application.motivational_letter_file_name "../Try It Out Doc.pdf"  end
  
  factory :application_recommended, :class => Application do |application|
    application.user_id 1
    application.job_id 1
    application.motivation "motivation" + Time.now.to_s
    application.state "recommended"
    application.cv_file_name "../Try It Out Doc.pdf"
    application.motivational_letter_file_name "../Try It Out Doc.pdf"
  end
  
  factory :application_dissuaded, :class => Application do |application|
    application.user_id 1
    application.job_id 1
    application.motivation "motivation" + Time.now.to_s
    application.state "dissuaded"
    application.cv_file_name "../Try It Out Doc.pdf"
    application.motivational_letter_file_name "../Try It Out Doc.pdf"
  end
  
  factory :application_hired, :class => Application do |application|
    application.user_id 1
    application.job_id 1
    application.motivation "motivation" + Time.now.to_s
    application.state "hired"
    application.cv_file_name "../Try It Out Doc.pdf"
    application.motivational_letter_file_name "../Try It Out Doc.pdf"
  end
  
  factory :application_declined, :class => Application do |application|
    application.user_id 1
    application.job_id 1
    application.motivation "motivation" + Time.now.to_s
    application.state "declined"
    application.cv_file_name "../Try It Out Doc.pdf"
    application.motivational_letter_file_name "../Try It Out Doc.pdf"
  end
  
  factory :application do |application|
    application.user_id 1
    application.job_id 1
    application.motivation "motivation" + Time.now.to_s
    application.state "created"
    application.cv_file_name "../Try It Out Doc.pdf"
    application.motivational_letter_file_name "../Try It Out Doc.pdf"
  end

end