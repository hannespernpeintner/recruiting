FactoryGirl.define do
  
  factory :company_complete, :class => Company do |company|
    company.name "testname" + Time.now.to_s
    company.template_header "testheader" + Time.now.to_s
    company.template_footer "testfooter" + Time.now.to_s
    company.contact_id 1
  end
  
  factory :company do |company|
    company.name "testname" + Time.now.to_s
    company.template_header "testheader" + Time.now.to_s
    company.template_footer "testfooter" + Time.now.to_s
    company.contact_id 1
  end

end