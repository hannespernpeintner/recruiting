FactoryGirl.define do
  
  factory :job_created, :class => Job do |job|
    job.title "testtitle" + Time.now.to_s
    job.description "testdescription" + Time.now.to_s
    job.tasks "testtasks" + Time.now.to_s
    job.qualifications "testqualis" + Time.now.to_s
    job.contact_id 1
    job.company_id 1
    job.state "created"
  end
  
  factory :job_reviewed, :class => Job do |job|
    job.title "testtitle" + Time.now.to_s
    job.description "testdescription" + Time.now.to_s
    job.tasks "testtasks" + Time.now.to_s
    job.qualifications "testqualis" + Time.now.to_s
    job.contact_id 1
    job.company_id 1
    job.state "reviewed"
  end
  
  factory :job_review_requested, :class => Job do |job|
    job.title "testtitle" + Time.now.to_s
    job.description "testdescription" + Time.now.to_s
    job.tasks "testtasks" + Time.now.to_s
    job.qualifications "testqualis" + Time.now.to_s
    job.contact_id 1
    job.company_id 1
    job.state "review_requested"
  end
  
  factory :job_published, :class => Job do |job|
    job.title "testtitle" + Time.now.to_s
    job.description "testdescription" + Time.now.to_s
    job.tasks "testtasks" + Time.now.to_s
    job.qualifications "testqualis" + Time.now.to_s
    job.contact_id 1
    job.company_id 1
    job.state "published"
  end
  
  factory :job do |job|
    job.title "testtitle" + Time.now.to_s
    job.description "testdescription" + Time.now.to_s
    job.tasks "testtasks" + Time.now.to_s
    job.qualifications "testqualis" + Time.now.to_s
    job.contact_id 1
    job.company_id 1
  end

end