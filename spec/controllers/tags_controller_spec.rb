require 'spec_helper'

describe TagsController do
  before (:all) do
    @tag = Tag.create
    @tag.name = "testname" + Time.now.to_s
    @tag.save
  end
  
  describe "GET index" do

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end
end