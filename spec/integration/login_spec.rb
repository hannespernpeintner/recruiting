describe 'login' do
  before(:all) do
    u = User.create
    u.email = "test@test.de"
    u.password = "start01"
    u.save
  end
  
  it 'logs the user in', :js => true, :slow => true do
    visit new_user_session_path
    fill_in 'user_email', :with => 'test@test.de'
    fill_in 'user_password', :with => 'start01'
    click_on 'login_submit'
    page.should have_content('Erfolgreich angemeldet')
  end
  
  it 'doesnt log the user in', :js => true, :slow => true do
    visit new_user_session_path
    fill_in 'user_email', :with => 'test@test.de'
    fill_in 'user_password', :with => 'start02'
    click_on 'login_submit'
    page.should have_content('Einloggen')
  end
  
end