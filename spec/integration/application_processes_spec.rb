require 'spec_helper'

describe 'Application' do
  # login before all activities here
  before(:all) do
    @company = FactoryGirl.build(:company)
    @company.save
    
    @job = FactoryGirl.build(:job_published)
    @job.save
    
    @admin = FactoryGirl.build(:admin)
    @admin.save
    
    @manager = FactoryGirl.build(:manager)
    @manager.save
    
    @applicant = FactoryGirl.build(:applicant)
    @applicant.save
    
    @hr = FactoryGirl.build(:hr)
    @hr.save
    
  end
  
  before(:each) do
    @application = FactoryGirl.build(:application_published)
    @application.save
  end
  
  def login_as(user)
    #visit signout_path
    #visit destroy_user_session_path
    #click_link "logoutlink" if page.has_content? "Logout"
    click_link "logoutlink" if page.has_link? "logoutlink"
    visit new_user_session_path
    
    fill_in 'user_email', :with => user.email
    fill_in 'user_password', :with => user.password
    click_on 'login_submit'
  end
  
  it 'can be done by applicant' do
    
    login_as(@applicant)
    visit job_path(2, @job.id)
    click_link 'apply_button'
    
    fill_in 'application_motivation', :with => "testmotivation"
    path = File.expand_path "spec/Try It Out Doc.pdf"
    
    attach_file 'application_motivational_letter', path
    attach_file 'application_cv', path
    
    click_on 'save_button'
    click_on 'publish_button'
    
    page.should have_content('Publish was successfull')
  end
  
  it 'validates input' do
    login_as(@applicant)
    visit job_path(2, @job.id)
    click_link 'apply_button'
    
    fill_in 'application_motivation', :with => "testmotivation"
    path = File.expand_path "spec/Try It Out Doc.pdf"
    wrong_path = File.expand_path "spec/spec_helper.rb"
    
    attach_file 'application_motivational_letter', path
    attach_file 'application_cv', wrong_path
    
    click_on 'save_button'
    sleep 0.1
    
    page.should have_css('#error_explanation')

    attach_file 'application_motivational_letter', path
    attach_file 'application_cv', path
    
    click_on 'save_button'
    click_on 'publish_button'
    sleep 0.1
    
    page.should have_content('Publish was successfull')
  end
  
  it 'completes hiring', :js => true, :slow => true do
    
    login_as(@hr)
    visit application_path(2, @application.id)
    click_link 'release_button'
    sleep 0.1
    @application.reload.released?.should be_true
    
    login_as(@manager)
    visit application_path(2, @application.id)
    click_link 'recommend_button'
    sleep 0.1
    @application.reload.recommended?.should be_true
    
    login_as(@hr)
    visit application_path(2, @application.id)
    click_link 'hire_button'
    sleep 0.1
    @application.reload.hired?.should be_true
  end
  
  it 'completes declining', :js => true, :slow => true do
    
    login_as(@hr)
    visit application_path(2, @application.id)
    click_link 'release_button'
    sleep 0.1
    @application.reload.released?.should be_true
    
    login_as(@manager)
    visit application_path(2, @application.id)
    click_link 'recommend_button'
    sleep 0.1
    @application.reload.recommended?.should be_true
    
    login_as(@hr)
    visit application_path(2, @application.id)
    click_link 'decline_button'
    sleep 0.1
    @application.reload.declined?.should be_true
  end
  
  
  self.use_transactional_fixtures = false
end