require 'spec_helper'

describe 'home page' do
  it 'displays the job index', :slow => true do
    visit '/'
    #page.should have_content('Stellen')
    page.should have_selector('.capybara-jobs')
  end
end