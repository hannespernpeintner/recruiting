require "spec_helper"

describe ApplicationMailer do
  before(:all) do
    @company = FactoryGirl.build(:company)
    @company.save
    
    @job = FactoryGirl.build(:job_published)
    @job.save
    
    @admin = FactoryGirl.build(:admin)
    @admin.save
    
    @manager = FactoryGirl.build(:manager)
    @manager.save
    
    @applicant = FactoryGirl.build(:applicant)
    @applicant.save
    
    @hr = FactoryGirl.build(:hr)
    @hr.save
    
  end
  
  def login_as(user)
    #visit signout_path
    #visit destroy_user_session_path
    #click_link "logoutlink" if page.has_content? "Logout"
    click_link "logoutlink" if page.has_link? "logoutlink"
    visit new_user_session_path
    
    fill_in 'user_email', :with => user.email
    fill_in 'user_password', :with => user.password
    click_on 'login_submit'
  end
  
  it 'sends acknowledgement mail' do
    
    login_as(@applicant)
    visit job_path(2, @job.id)
    click_link 'apply_button'
    
    fill_in 'application_motivation', :with => "testmotivation"
    path = File.expand_path "spec/Try It Out Doc.pdf"
    
    attach_file 'application_motivational_letter', path
    attach_file 'application_cv', path
    
    click_on 'save_button'
    click_on 'publish_button'
    
    ActionMailer::Base.deliveries.last.to.should == [@applicant.email]
    ActionMailer::Base.deliveries.last.subject.should == 'Your application has been placed successfully'
    
    page.should have_content('Publish was successfull')
  end
end
