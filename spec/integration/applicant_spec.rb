require 'spec_helper'

describe 'applicant' do
  # login before all activities here
  before(:all) do
    @company = FactoryGirl.build(:company)
    @company.save
    
    @job = FactoryGirl.build(:job_published)
    @job.save
    
    @applicant = FactoryGirl.build(:applicant)
    @applicant.save
    
    visit new_user_session_path
    fill_in 'user_email', :with => @applicant.email
    fill_in 'user_password', :with => @applicant.password
    click_on 'login_submit'
  end
  
  it 'cannot see the manage buttons', :js => true, :slow => true do
    visit jobs_path
    page.has_no_link?('job_edit_button')
    page.has_no_link?('job_destroy_button')
  end
  
end