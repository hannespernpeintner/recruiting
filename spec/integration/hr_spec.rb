require 'spec_helper'

describe 'hr' do
  # login before all activities here
  before(:all) do
    @company = FactoryGirl.build(:company)
    @company.save
    
    @job = FactoryGirl.build(:job_published)
    @job.save
    
    @hr = FactoryGirl.build(:hr)
    @hr.save
    
    visit new_user_session_path
    fill_in 'user_email', :with => @hr.email
    fill_in 'user_password', :with => @hr.password
    click_on 'login_submit'
  end
  
  it 'can see the manage buttons', :js => true, :slow => true do
    visit jobs_path
    page.has_link?('job_edit_button')
    page.has_link?('job_destroy_button')
  end
  
end