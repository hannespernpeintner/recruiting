require 'spec_helper'

describe 'Job' do
  # login before all activities here
  before(:all) do
    @company = FactoryGirl.build(:company)
    @company.save
    
    @manager = FactoryGirl.build(:manager)
    @manager.save
    
    @hr = FactoryGirl.build(:hr)
    @hr.save
    
  end
  
  def login_as(user)
    click_link "logoutlink" if page.has_content? "Logout"
    visit new_user_session_path
    
    fill_in 'user_email', :with => user.email
    fill_in 'user_password', :with => user.password
    click_on 'login_submit'
  end
  
  it 'completes job publication', :js => true, :slow => true do
    
    title = "testtititel" + Time.now.to_s
    description = "testdescription"+ Time.now.to_s
    
    login_as(@hr)
    
    sleep 0.1
    
    click_link 'new_job_button'
    fill_in 'job_title', :with => title
    fill_in 'job_description', :with => description
    select(@hr.email, :from => "job_contact_id")
    #select(@company.name, :from => "job_company_id")
    find(:select, "job_company_id").first(:option, @company.name).select_option
    click_on 'submit_button'
    
    
    sleep 0.1
    
    job = Job.where("title = :title AND description = :description", {title: title, description: description}).first
    job.should_not be_nil
    job.created?.should be_true
    
    
    click_link 'request_review_button'
    
    sleep 0.1
    
    job.reload.review_requested?.should be_true
    
    login_as(@manager)
    visit job_path(2, job.id)
    click_link "review_button"
    
    sleep 0.1
    
    job.reload.reviewed?.should be_true
    
    login_as(@hr)
    visit job_path(2, job.id)
    click_link "publish_button"
    
    sleep 0.1
    
    job.reload.published?.should be_true
    
  end
  
  self.use_transactional_fixtures = false
end